# Food recognition in python with analysis of information (food, category, nutrients and color)

## Recognize food with: 
- Python;
- Google vision;
- OpenCV.

## Required API to translate name of the food:
- Watson Language Translator.

## Requirements:
- Python 3;
- GCP account (To use google vision);
- OpenCV (To scale image only);
- IBM sccount (To translate the food name from english to portuguese).

## Required libraries
- pip install --upgrade google-cloud-vision
- pip install --upgrade opencv-python
- pip install --upgrade requests
- pip install --upgrade Flask
- pip install --upgrade pandas
- pip install --upgrade numpy
- pip install --upgrade ibm-watson