"""
INTEGRATED PROJECT A and B
Food Recognition API with Google Vision and OpenCv
"""

import io, os, json, cv2
import pandas as pd
import numpy as np
from google.cloud import vision_v1p3beta1 as vision

# Setup google authen client key
try:
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'client_key.json'
except:
    print("Error to open the json file!")

# Source path of Dataset
DATASEY_SOURCE_PATH = "./dataset/food_informations.csv"  # Dataset with all food informations

class Food:
    def __init__(self):
        self.Recognized_Foods = []

def load_date_of_dataset(date_name):
    dates = pd.read_csv(DATASEY_SOURCE_PATH, sep=";")
    dates = dates.replace(np.nan, '', regex=True)
    dates = dates[date_name].values
    dates = [element.strip() for element in dates]
    dates = [element.lower() for element in dates]
    return dates

def food_recognition(image_path):
    try:
        foods_list = load_date_of_dataset("food")
        translated_foods_list = load_date_of_dataset("comida")
    except:
        print("Error to load the foods list!")
    
    try:
        # Read image with openCv library
        food_image = cv2.imread(image_path)

        # Get image size (height and width) 
        height, width = food_image.shape[:2]

        # Scale image height = 800 and new width = height * 800 / old width 
        food_image = cv2.resize(food_image, (800, int((height * 800) / width)))

        # Save the image to temp file in same local of the file food_recognition.py
        cv2.imwrite("./image.jpg", food_image)

        # Create new img path for the Google Vision
        image_path = "./image.jpg"

        # Create Google Vision Client
        client = vision.ImageAnnotatorClient()

        # Read cut image file for openCv
        with io.open(image_path, 'rb') as image_file:
            content = image_file.read()
        image = vision.types.Image(content=content)

        # Returning a maximum of 20 suggestions
        recognize_text = client.label_detection(image=image, max_results=20)
        labels = recognize_text.label_annotations

        # Remove temporary image
        os.remove(image_path)

        # Instantiating a object (recognized foods)
        recognized_foods_list = Food()

        # Verify if a recognized food belongs to our dataset
        for label in labels:
            food_description = label.description.lower()
            if (food_description in foods_list):
                index = foods_list.index(food_description)
                recognized_foods_list.Recognized_Foods.append(translated_foods_list[index].title())
                
        # Create a json file with recognized foods
        recognized_foods_json = json.dumps(recognized_foods_list.__dict__)
        return recognized_foods_json 
    except:
        print("Error in the image path or to read the image!")
        return None