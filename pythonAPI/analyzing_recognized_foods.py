"""
INTEGRATED PROJECT A and B
Food Recognition API with Google Vision and OpenCv
"""

import json
import pandas as pd
import numpy as np

# Source path of Dataset
DATASEY_SOURCE_PATH = "./dataset/food_informations.csv"  # Dataset with all food informations

class RecognizedFoodInformations:
    def __init__(self):
        self.Food = []
        self.Category = []
        self.Color = []

def load_date_of_dataset(date_name):
    dates = pd.read_csv(DATASEY_SOURCE_PATH, sep=";")
    dates = dates.replace(np.nan, '', regex=True)
    dates = dates[date_name].values
    dates = [element.strip() for element in dates]
    dates = [element.lower() for element in dates]
    return dates

def get_information_from_recognized_foods(recognized_foods_json):
    try:
        translated_foods_list = load_date_of_dataset("comida")
        categories_list = load_date_of_dataset("categoria")
        color_list = load_date_of_dataset("cor")
    except:
        print("Error to load some list (food, category or color)!")    
    
    try:
        recognized_foods_list = recognized_foods_json['Recognized_Foods']
        
        # Create a list to save objects (Recognized Food Information)
        recognized_food_information_list = []

        # instantiating a object (food, category and color)
        recognized_foods_informations = RecognizedFoodInformations()
        
        # Get recognized foods informations
        for food in recognized_foods_list:
            food = food.lower()
            if (food in translated_foods_list):
                index = translated_foods_list.index(food)
                    
                # Create a objects list with food informations to create a json file
                recognized_foods_informations.Food = food.title()
                recognized_foods_informations.Category = categories_list[index].title()
                recognized_foods_informations.Color = color_list[index].title()
                food_information = recognized_foods_informations.__dict__.copy()
                recognized_food_information_list.append(food_information)

        # Create the json file with food, category and color and return this json
        recognized_foods_informations_json = json.dumps(recognized_food_information_list, ensure_ascii=False, indent=2, separators=(',',':'))    
        return recognized_foods_informations_json
    except:
        print("Error to load the food list in json file!")