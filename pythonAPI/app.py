"""
INTEGRATED PROJECT A and B
Food Recognition API with Google Vision and OpenCv
"""

import os
from flask import Flask, request, jsonify
from food_recognition import food_recognition
from analyzing_recognized_foods import get_information_from_recognized_foods

app = Flask(__name__)

# Communication endpoint to send a image and to receive a json file with recognized foods
@app.route("/post_image", methods=["POST"])
def send_photographed_image():
    try:
        image = request.files['image']
        try:
            path = image.filename.split('.')
            path = "img." + path[len(path)-1]
            image.save(path)
            recognized_foods = food_recognition(path)
            
            # Remove temporary image
            os.remove(path)

            # Return json file with recognized foods
            return recognized_foods
        except:
            print("Error in path to save the new image!")
            return None
    except:
        print("Error in file1 requesting!")
        return None

# Communication endpoint to send a json file and to receive a json file with foods informations of input file
@app.route("/post_recognized_foods", methods=["POST"])
def send_recognized_foods():
    try:
        recognized_foods_json = request.json
        recognized_foods_informations_json = get_information_from_recognized_foods(recognized_foods_json)
        
        # Return json file with recognized foods informations
        return recognized_foods_informations_json
    except:
        print("Error in request json!")
        return None

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8090, debug=False)